import unittest
from calculadora import Calculadora

class TestCalculadora(unittest.TestCase):
    def setUp(self):
        self.cal=Calculadora()
    
    def test_suma_dos_mas_dos(self):
        resultado = self.cal.sumar(2,2)
        self.assertEqual(4,resultado)
        
        
    """
    def test_suma_diez_mas_tres(self):
        resultado = self.cal.sumar(10,3)
        self.assertEqual(13,resultado)
    
    def test_suma_x_mas_tres(self):
        resultado = self.cal.sumar('X',3)
        self.assertEqual('solo se aceptan numeros',resultado)
    
    def test_suma_menos_dos_mas_tres(self):
        resultado = self.cal.sumar(-2,3)
        self.assertEqual('solo se aceptan numeros positivos',resultado)
    
    def test_suma_uno_mas_nuevePuntoCinco(self):
        resultado = self.cal.sumar(1,9.5)
        self.assertEqual('solo numeros enteros',resultado)
    
    def test_suma_nuevePuntoCinco_mas_1(self):
        resultado = self.cal.sumar(9.5,1)
        self.assertEqual('solo numeros enteros',resultado)
        
    def test_suma_lista_mas_9(self):
        resultado = self.cal.sumar([0,8],9)
        self.assertEqual('solo numeros enteros, no listas',resultado)

    def test_suma_nueve_mas_(self):
        resultado = self.cal.sumar(9,[0,8])
        self.assertEqual('solo numeros enteros, no listas',resultado)
        
        
        
    def test_resta_dos_menos_dos(self):
        resultado = self.cal.resta(3,10)
        self.assertEqual(-7,resultado)
    
    def test_resta_diez_menos_tres(self):
        resultado = self.cal.resta(10,3)
        self.assertEqual(7,resultado)
    
    def test_resta_x_menos_tres(self):
        resultado = self.cal.resta('X',3)
        self.assertEqual('solo se aceptan numeros',resultado)
    
    def test_resta_menos_dos_menos_tres(self):
        resultado = self.cal.resta(-2,3)
        self.assertEqual('solo se aceptan numeros positivos',resultado)
    
    def test_resta_uno_menos_nuevePuntoCinco(self):
        resultado = self.cal.resta(1,9.5)
        self.assertEqual('solo numeros enteros',resultado)
    
    def test_resta_nuevePuntoCinco_menos_1(self):
        resultado = self.cal.resta(9.5,1)
        self.assertEqual('solo numeros enteros',resultado)
        
    def test_resta_lista_menos_9(self):
        resultado = self.cal.resta([0,8],9)
        self.assertEqual('solo numeros enteros, no listas',resultado)

    def test_resta_nueve_menos_(self):
        resultado = self.cal.resta(9,[0,8])
        self.assertEqual('solo numeros enteros, no listas',resultado)
        
    
    
    
    def test_multiplicar_dos_por_dos(self):
        resultado = self.cal.multiplicar(3,10)
        self.assertEqual(30,resultado)
    
    def test_multiplicar_x_por_tres(self):
        resultado = self.cal.multiplicar('X',3)
        self.assertEqual('solo se aceptan numeros',resultado)
    
    def test_multiplicar_por_dos_por_tres(self):
        resultado = self.cal.multiplicar(-2,3)
        self.assertEqual('solo se aceptan numeros positivos',resultado)
    
    def test_multiplicar_uno_por_nuevePuntoCinco(self):
        resultado = self.cal.multiplicar(1,9.5)
        self.assertEqual('solo numeros enteros',resultado)
    
    def test_multiplicar_nuevePuntoCinco_por_1(self):
        resultado = self.cal.multiplicar(9.5,1)
        self.assertEqual('solo numeros enteros',resultado)
        
    def test_multiplicar_lista_por_9(self):
        resultado = self.cal.multiplicar([0,8],9)
        self.assertEqual('solo numeros enteros, no listas',resultado)

    def test_multiplicar_nueve_por_(self):
        resultado = self.cal.multiplicar(9,[0,8])
        self.assertEqual('solo numeros enteros, no listas',resultado)
        
        
    
    def test_dividir_diez_entre_tres(self):
        resultado = self.cal.dividir(10,3)
        self.assertEqual(3.3333333333333335,resultado)
    
    def test_dividir_x_entre_tres(self):
        resultado = self.cal.dividir('X',3)
        self.assertEqual('solo se aceptan numeros',resultado)
    
    def test_dividir_menos_dos_entre_tres(self):
        resultado = self.cal.dividir(-2,3)
        self.assertEqual('solo se aceptan numeros positivos',resultado)
    
    def test_dividir_uno_entre_nuevePuntoCinco(self):
        resultado = self.cal.dividir(1,9.5)
        self.assertEqual('solo numeros enteros',resultado)
    
    def test_dividir_nuevePuntoCinco_entre_1(self):
        resultado = self.cal.dividir(9.5,1)
        self.assertEqual('solo numeros enteros',resultado)
        
    def test_dividir_lista_entre_9(self):
        resultado = self.cal.dividir([0,8],9)
        self.assertEqual('solo numeros enteros, no listas',resultado)

    def test_dividir_nueve_entre_(self):
        resultado = self.cal.dividir(9,[0,8])
        self.assertEqual('solo numeros enteros, no listas',resultado)
        
    
    
    def test_potencia_diez_conPotenciaDe_tres(self):
        resultado = self.cal.potencia(5,2)
        self.assertEqual(25,resultado)
    
    def test_potencia_x_conPotenciaDe_tres(self):
        resultado = self.cal.potencia('X',3)
        self.assertEqual('solo se aceptan numeros',resultado)
    
    def test_potencia_menos_dos_conPotenciaDe_tres(self):
        resultado = self.cal.potencia(-2,3)
        self.assertEqual('solo se aceptan numeros positivos',resultado)
    
    def test_potencia_uno_conPotenciaDe_nuevePuntoCinco(self):
        resultado = self.cal.potencia(1,9.5)
        self.assertEqual('solo numeros enteros',resultado)
    
    def test_potencia_nuevePuntoCinco_conPotenciaDe_1(self):
        resultado = self.cal.potencia(9.5,1)
        self.assertEqual('solo numeros enteros',resultado)
        
    def test_potencia_lista_conPotenciaDe_9(self):
        resultado = self.cal.potencia([0,8],9)
        self.assertEqual('solo numeros enteros, no listas',resultado)

    def test_potencia_nueve_conPotenciaDe_(self):
        resultado = self.cal.potencia(9,[0,8])
        self.assertEqual('solo numeros enteros, no listas',resultado)
        
        
        
    def test_raiz_de_nueve(self):
        resultado = self.cal.raiz(9)
        self.assertEqual(3.0,resultado)
    
    def test_raiz_de_x(self):
        resultado = self.cal.raiz('X')
        self.assertEqual('solo se aceptan numeros',resultado)
    
    def test_raiz_de_menos_dos(self):
        resultado = self.cal.raiz(-2)
        self.assertEqual('solo se aceptan numeros positivos',resultado)
    
    def test_raiz_de_nueve_punto_cinco(self):
        resultado = self.cal.raiz(9.5)
        self.assertEqual('solo numeros enteros',resultado)

    def test_raiz_de_lista_(self):
        resultado = self.cal.raiz([0,8])
        self.assertEqual('solo numeros enteros, no listas',resultado)
"""
if __name__ == '__main__': #pragma: no cover
    unittest.main()