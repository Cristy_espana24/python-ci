import math

class Calculadora:
    def sumar(self, num1, num2):
        try:
            if isinstance(num1, list) or isinstance(num2, list):
                return "solo numeros enteros, no listas"
            if (num1 > 0 and num2 > 0):
                if (type(num1) == int and type(num2) == int):
                    return num1 + num2
                else:
                    return 'solo numeros enteros'
            else:
                return 'solo se aceptan numeros positivos'
        except:
            return 'solo se aceptan numeros'

    def resta(self, num1, num2):
        try:
            if isinstance(num1, list) or isinstance(num2, list):
                return "solo numeros enteros, no listas"
            if (num1 > 0 and num2 > 0):
                if (type(num1) == int and type(num2) == int):
                    return num1 - num2
                else:
                    return 'solo numeros enteros'
            else:
                return 'solo se aceptan numeros positivos'
        except:
            return 'solo se aceptan numeros'

    def multiplicar(self, num1, num2):
        try:
            if isinstance(num1, list) or isinstance(num2, list):
                return "solo numeros enteros, no listas"
            if (num1 > 0 and num2 > 0):
                if (type(num1) == int and type(num2) == int):
                    return num1 * num2
                else:
                    return 'solo numeros enteros'
            else:
                return 'solo se aceptan numeros positivos'
        except:
            return 'solo se aceptan numeros'

    def dividir(self, num1, num2):
        try:
            if isinstance(num1, list) or isinstance(num2, list):
                return "solo numeros enteros, no listas"
            if (num1 > 0 and num2 > 0):
                if (type(num1) == int and type(num2) == int):
                    return num1 / num2
                else:
                    return 'solo numeros enteros'
            else:
                return 'solo se aceptan numeros positivos'
        except:
            return 'solo se aceptan numeros'

    def potencia(self, num1, num2):
        try:
            if isinstance(num1, list) or isinstance(num2, list):
                return "solo numeros enteros, no listas"
            if (num1 > 0 and num2 > 0):
                if (type(num1) == int and type(num2) == int):
                    return pow(num1, num2)
                else:
                    return 'solo numeros enteros'
            else:
                return 'solo se aceptan numeros positivos'
        except:
            return 'solo se aceptan numeros'

    def raiz(self, num):
        try:
            if isinstance(num, list):
                return "solo numeros enteros, no listas"
            if (num > 0):
                if (type(num) == int):
                    return math.sqrt(num)
                else:
                    return 'solo numeros enteros'
            else:
                return 'solo se aceptan numeros positivos'
        except:
            return 'solo se aceptan numeros'
'''
python -m doctest calculadora.py -v

python -m doctest test_calculadora.txt -v

'''
